// Ignat Eduard Gabriel
// subiect 2

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class subiect2 extends JFrame {
    JButton button;
    JTextField textField1;
    JTextField textField2;
    JLabel label1,label2;

    subiect2(){
        setTitle("subiect 2 app");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setVisible(true);
        setSize(300,200);
    }

    void init(){
        this.setLayout(null);
        textField1 = new JTextField();
        textField1.setBounds(100,10,100, 20);
        textField1.setEditable(true);

        label1 = new JLabel("Field1:");
        label1.setBounds(10,10,70,20);

        textField2 = new JTextField();
        textField2.setBounds(100,40,100, 20);
        textField2.setEditable(true);

        label2 = new JLabel("Field2:");
        label2.setBounds(10,40,70,20);

        button = new JButton();
        button.setBounds(10, 70, 100, 20);
        button.setText("Print");
        button.addActionListener(event -> buttonReaction(event));

        add(textField1);
        add(textField2);
        add(label1);
        add(label2);
        add(button);
    }

    void buttonReaction(ActionEvent event){

        String filename = textField1.getText();// getting the filename for the text file
        try {
            File myFile = new File(filename);
            if (myFile.createNewFile()) {
                System.out.println("File created: " + myFile.getName()); //if the file doesn't exist we create it
            } else {
                System.out.println("File already exists.");
            }
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

        try {
            FileWriter myWriter = new FileWriter(filename);
            String text = textField2.getText();//getting the text from the second text field
            myWriter.write(text);//writing in the file
            myWriter.close();
            System.out.println("Successfully wrote to the file.");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new subiect2();
    }
}
