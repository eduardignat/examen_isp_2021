// Ignat Eduard Gabriel
// subiect 1



public class subiect1{

   class U{
       public void p(){}
   }

   class N{
       private I i; //uni-directional dependency
   }

   class I extends U{ // inheritance
       private long t;
       public K k;//uni-directional dependency
       public void f(){}

   }

   class J{
       public void i(I I){}; // dependency
   }

   class K{
       public L l;// aggregation with L
       public M m;// composition with M

       K(){
           this.l = new L(); // for aggregation with L
       }

       void setM(M m){
           this.m = m; // for composition with M
       }
   }

   class L{
       public void metA(){}
   }

   class M{
       public void metB(){}
   }
}
